# Sphinx Autobuild Docker image
#
# Copyright (C) 2023 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PYTHON_IMAGE_TAG=3.11.5-alpine3.18
FROM python:${PYTHON_IMAGE_TAG}
LABEL org.opencontainers.image.authors='Libre Space Foundation <info@libre.space>'

ARG SPHINX_AUTOBUILD_PORT=8000
ARG SPHINX_AUTOBUILD_SOURCEDIR=/docs
ARG SPHINX_AUTOBUILD_OUTDIR=/html

ENV PYTHONUNBUFFERED=1

# Copy packages list
COPY packages.alpine /usr/local/src/docs/

# Install system packages
RUN xargs -a /usr/local/src/docs/packages.alpine apk add --no-cache -qU git

# Copy requirements files
COPY requirements.txt /usr/local/src/docs/
COPY requirements-dev.txt /usr/local/src/docs/
COPY constraints.txt /usr/local/src/docs/

# Install requirements
RUN pip install \
    --no-cache-dir \
    -c /usr/local/src/docs/constraints.txt \
    -r /usr/local/src/docs/requirements.txt \
    -r /usr/local/src/docs/requirements-dev.txt

# Create directories and volumes
RUN install -d \
	${SPHINX_AUTOBUILD_SOURCEDIR} \
	${SPHINX_AUTOBUILD_OUTDIR}
VOLUME ["${SPHINX_AUTOBUILD_SOURCEDIR}", "${SPHINX_AUTOBUILD_OUTDIR}"]

# Add exception for Git error
RUN git config --global --add safe.directory "${SPHINX_AUTOBUILD_SOURCEDIR}"

# Expose server port
EXPOSE ${SPHINX_AUTOBUILD_PORT}

# Run server
ENV SPHINX_AUTOBUILD_PORT ${SPHINX_AUTOBUILD_PORT}
ENV SPHINX_AUTOBUILD_SOURCEDIR ${SPHINX_AUTOBUILD_SOURCEDIR}
ENV SPHINX_AUTOBUILD_OUTDIR ${SPHINX_AUTOBUILD_OUTDIR}
CMD sphinx-autobuild \
    --port "${SPHINX_AUTOBUILD_PORT}" \
    --host 0.0.0.0 \
    --re-ignore '^'"${SPHINX_AUTOBUILD_SOURCEDIR}"'/(?!.+\.(rst|j2|svg|png)$|(conf|_version|_ext/.+)\.py$|setup\.(cfg|py)$)' \
    "${SPHINX_AUTOBUILD_SOURCEDIR}" \
    "${SPHINX_AUTOBUILD_OUTDIR}"
